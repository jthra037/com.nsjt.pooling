Pooling in Unity, made extensible through specialized storage and object creation/destruction structs, assembled into a base nearly as efficient as direct access through the use of generics.

- - - -

### Usage
Basic access through `IPool<T>` interface:
```c#
	public interface IPool<T>
	{
		T Get();
		void Release(T obj);
	}
```

The generic base `SPool<T, F, G>` has three generic parameters, which correspond to:
- `T`: The type which is being pooled
- `F`: a `struct` which implements `IValueFactory<T>`, which gives specifics on how new `T` objects should be created if the pool is empty and another value is requested (as well as how an object should be destroyed if it is released back to the pool and the pool is full)
- `G`: a `struct `which implements `IStore<T>`, which specifies how to use the underlying collection that the pool runss on.

The pool is constructed with a given size, and starts empty. The pool can be "Released" values using `Release(T obj)` or allowed to construct it's own `T` objects by calling `Get()` on the empty pool and releasing the results back to the pool when they are no longer in use. 

When a value is requested from an empty pool, the `IValueFactory` will create a new value and return it. When a value is released to the pool and the pool is full, that object will be dealth with according to the `IValueFactory`.

An example implementation, `PrefabPool` has been included which also makes use of Unity serialization to follow to standard practice of connecting prefab references in the editor for instantiation, and providing pool size through the inspector.

- - - -
### Extension
New `IValueFactory<T>` and `IStore<T>` structs can be implemented, and then given directly to the generic `SPool` (slightly better for performance) or an inherited version can specify the factory and store (allowing for Unity serialization).

#### Included `IValueFactory<T>`
- `PrefabFactory` : `IValueFactory<GameObject>`
	- (May) Use Unity serialization to allow for referencing a prefab `GameObject` and an optional parent `Transform`. Instantiates a given prefab object childed to parent, and destroys GameObject's if requested.
- `GOFactory` : `IValueFactory<GameObject>`
	- Creates `new GameObject`
#### Included `IStore<T>`
- ArrayStore<T> : IStore<T>
	- Runs an `IStore` using an array of `T`