﻿// ========================================================================================
// Pooling - Efficient and extensible pooling for Unity using C# generics
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

namespace NSJT.Pooling
{
	public interface IStore<T>
	{
		int Count { get; }
		bool HasRoom();
		bool HasValues();
		void Add(T obj);
		T Remove();
		void Initialize(int size);
	}
}