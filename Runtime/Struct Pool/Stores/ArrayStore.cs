﻿// ========================================================================================
// Pooling - Efficient and extensible pooling for Unity using C# generics
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;

namespace NSJT.Pooling.Stores
{
	public struct ArrayStore<T> : IStore<T>
	{
		private int size;
		private int count;
		private T[] store;

		public int Count => count;

		public bool HasRoom()
		{
			return count < size;
		}

		public bool HasValues()
		{
			return count >= 0;
		}

		public void Add(T obj)
		{
			store[++count] = obj;
		}

		public T Remove()
		{
			return store[count--];
		}

		public void Initialize(int size)
		{
			this.size = size;
			if (store == null)
			{
				initialize(size);
			}
			else
			{
				resize(size);
			}
		}

		private void resize(int size)
		{
			var temp = new T[size];
			int maxID = size < store.Length ? size : store.Length;
			Array.Copy(store, temp, maxID);
			if (count >= size)
			{
				count = temp.Length - 1;
			}
			store = temp;
		}

		private void initialize(int size)
		{
			store = new T[size];
			count = -1;
		}
	}
}