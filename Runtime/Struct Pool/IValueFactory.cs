﻿// ========================================================================================
// Pooling - Efficient and extensible pooling for Unity using C# generics
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

namespace NSJT.Pooling
{
	public interface IValueFactory<T>
	{
		T Create();
		void Destroy(T obj);
	}
}