﻿// ========================================================================================
// Pooling - Efficient and extensible pooling for Unity using C# generics
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using UnityEngine;

namespace NSJT.Pooling.ValueFactories
{
	public struct GOFactory : IValueFactory<GameObject>
	{
		public GameObject Create()
		{
			return new GameObject();
		}

		public void Destroy(GameObject obj)
		{
			Object.Destroy(obj);
		}
	}
}