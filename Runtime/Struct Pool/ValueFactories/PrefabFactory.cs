﻿// ========================================================================================
// Pooling - Efficient and extensible pooling for Unity using C# generics
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using UnityEngine;

namespace NSJT.Pooling.ValueFactories
{
	[Serializable]
	public struct PrefabFactory : IValueFactory<GameObject>
	{
#pragma warning disable CS0649
		[SerializeField]
		private GameObject prefab;
		[SerializeField]
		private Transform parent;
#pragma warning restore CS0649

		private Vector3 position =>
			parent == null ?
			Vector3.zero : parent.position;

		private Quaternion rotation =>
			parent == null ?
			Quaternion.identity : parent.rotation;

		public GameObject Create()
		{
			return UnityEngine.Object.Instantiate(prefab, position, rotation, parent);
		}

		public void Destroy(GameObject obj)
		{
			Destroy(obj);
		}
	}
}
