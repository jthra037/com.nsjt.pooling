﻿// ========================================================================================
// Pooling - Efficient and extensible pooling for Unity using C# generics
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using UnityEngine;
using NSJT.Pooling.Stores;
using NSJT.Pooling.ValueFactories;

namespace NSJT.Pooling
{
	[Serializable]
	public class PrefabPool : SPool<GameObject,
	PrefabFactory,
	ArrayStore<GameObject>>,
	ISerializationCallbackReceiver
	{
#pragma warning disable CS0649
		[SerializeField]
		private int _size;
#pragma warning restore CS0649
		public PrefabPool(int size) : base(size) { }

		public void OnAfterDeserialize()
		{
			setSize(_size);
		}

		public void OnBeforeSerialize() { }
	}
}
