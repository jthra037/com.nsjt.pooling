﻿// ========================================================================================
// Pooling - Efficient and extensible pooling for Unity using C# generics
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using UnityEngine;

namespace NSJT.Pooling
{
	[Serializable]
	public class SPool<T, F, G> : IPool<T>
	where F : struct, IValueFactory<T>
	where G : struct, IStore<T>
	{
		private int size;
		private int count = -1;

		[SerializeField]
		private F valueFactory = default;
		[SerializeField]
		private G store = default;

		public SPool(int size)
		{
			setSize(size);
		}

		public T Get()
		{
			T result;

			if (count >= 0)
			{
				result = store.Remove();
				count--;
			}
			else
			{
				result = valueFactory.Create();
			}

			return result;
		}

		public void Release(T obj)
		{
			if (count < size)
			{
				count++;
				store.Add(obj);
			}
			else
			{
				valueFactory.Destroy(obj);
			}
		}

		protected void setSize(int size)
		{
			this.size = size;
			store.Initialize(size);
			count = store.Count;
		}
	}
}
